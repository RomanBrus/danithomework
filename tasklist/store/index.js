export const state = () => ({
  notes: []
})

export const getters = {
  getNotes: (state) => {
    return state.notes
  },
}

export const mutations = {
  ADD_TASK(state, note) {
    state.notes = [{
      id: new Date().getTime().toString(),
      name: note.name,
      itemText: note.itemText,
      done: note.done,
    }, ...state.notes]
  },

  REMOVE_TASK(state, id) {
    state.notes = state.notes.filter(note => id !== note.id);
  },

  EDIT_TASK(state, note) {
    const index = state.notes.findIndex(t => t.id === note.id);
    Object.assign(state.notes[index], note)
  },

  CHANGE_STATUS(state, note) {
    const index = state.notes.findIndex(t => t.id === note.id);
    note.done = !note.done
    Object.assign(state.notes[index], note)
  }
}
