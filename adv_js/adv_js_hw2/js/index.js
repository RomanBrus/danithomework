"use strict";

const books = [{
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

class NotValidBook extends Error {
    constructor(text) {
        super();
        this.name = "NotValidBook";
        this.message = `${text}`;
    }
}

class BookItem {
    constructor(book) {
        this.book = book;
    }

    render(container) {
        container.insertAdjacentHTML('beforeend', `
        <ul>
            <li>${this.book.author} ${this.book.name} ${this.book.price}</li>
        </ul>`);
    }
}
const container = document.getElementById("root");

books.forEach((item, index) => {
    try {
        if (!item.author) {
            throw new NotValidBook(`No author ${index}`);
        }
        if (!item.name) {
            throw new NotValidBook(`No name ${index}`);
        }
        if (!item.price) {
            throw new NotValidBook(`No price ${index}`);
        }
        new BookItem(item).render(container);
    } catch (err) {
        console.log(err.message);
    }
})