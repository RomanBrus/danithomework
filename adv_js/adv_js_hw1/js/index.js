"use strict";

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this._salary = salary;
    }

    get salary() {
        return this._salary;
    }

    set salary(value) {
        this._salary = value;
    }

    get age() {
        return this._age;
    }

    set age(age) {
        this._age = age;
    }

    get name() {
        return this._name;
    }

    set name(name) {
        this._name = name;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    get salary() {
        return super.salary * 3;
    }
}

const programmerFirst = new Programmer("Dima", 36, 4000, ["HTML, PHP"]);
console.log(programmerFirst);

const programmerSecond = new Programmer("Petya", 39, 4500, ["JS", "HTML, PHP"]);
console.log(programmerSecond);

const programmerThird = new Programmer("Ihor", 42, 5000, ["CSS, RUBY"]);
console.log(programmerThird);