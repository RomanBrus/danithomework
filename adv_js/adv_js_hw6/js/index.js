"use strict";

const urlIp = 'https://api.ipify.org/?format=json';
const urlData = 'https://ip-api.com/';
const loader = document.querySelector('.btn');

loader.addEventListener('click', (e) => {
    e.preventDefault();
    const a = getData();
    a.then(data => {
        const {
            timezone,
            country,
            regionName,
            city,
        } = data;

        document.querySelector('.information').insertAdjacentHTML('beforeend', `
            <p>Timezone: ${timezone}</p>
            <p>Country: ${country}</p>
            <p>Region: ${regionName}</p>
            <p>City: ${city}</p>`)
    })
});

const getIp = async () => {
    try {
        const ip = await fetch(urlIp).then(response => response.json());
        return ip;
    } catch (err) {
        console.error(err);
    }
};

const getData = async () => {
    const {
        ip
    } = await getIp();

    try {
        const data = await fetch(`http://ip-api.com/json/${ip}`).then(response => response.json());

        return data;
    } catch (err) {
        console.error(err);
    }
};