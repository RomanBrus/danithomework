"use strict";

const main = document.body;
const formPrice = document.createElement("form");
formPrice.textContent = "Рrice, $";
document.body.append(formPrice);
formPrice.style = "background: lightblue; width: 250px; padding-left: 20px;";

const inputPrice = document.createElement("input");
inputPrice.placeholder = "Please enter price";
formPrice.append(inputPrice);
inputPrice.style.margin = "10px";

inputPrice.addEventListener("focus", focusOn);
function focusOn(event) {
    inputPrice.style.border = "1px solid green";
};

inputPrice.addEventListener("blur", blurOn);
function blurOn(event) {
    if (inputPrice.value !== "" && typeof +inputPrice.value === "number" && !isNaN(inputPrice.value)) {
        inputPrice.style.color = "green";
    
        const divSpan = document.createElement("div");
        main.prepend(divSpan);
        divSpan.style = "display: inline-block; justify-content:center;border: 1px solid darkblue; border-radius:20px; padding: 3px; font-size: 10px; margin-bottom:15px";
        
        const spanPrice = document.createElement("span");
        const close = document.createElement("button");
        spanPrice.textContent = `Текущая цена: ${inputPrice.value}`;
        spanPrice.style = "margin-right: 5px";
        divSpan.prepend(spanPrice);

        close.textContent = "X";
        close.style = "border: 1px solid darkblue; border-radius:50%; font-size: 14px;";
        spanPrice.after(close);

        close.addEventListener("click", closeOn);
        function closeOn(event) {
            inputPrice.value = "";
            divSpan.style.display = "none";
        };

        if (+inputPrice.value < 0) {
            inputPrice.style.border = "1px solid red";
            inputPrice.insertAdjacentText("afterend", `Please enter correct price`);
            closeOn();
        };
    }
};

