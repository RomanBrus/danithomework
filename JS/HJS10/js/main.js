"use strict";

const passForm = document.getElementById('wrapper');

passForm.addEventListener("click", function (e) {
    if (e.target.closest("i")) {
        e.target.previousElementSibling.type = "text";
        e.target.classList.replace("fa-eye", "fa-eye-slash");
    }
    if (e.target.closest("button")) {
        const inputs = document.querySelectorAll("input");
        const err = document.getElementById("err");
        const pass = document.getElementById("pass").value;
        const passCheck = document.getElementById("pass-check").value;
        
        if (pass === passCheck && !(pass === "") && !(passCheck === "")) {
            alert("You are welcome");
            err.remove();
        } else {
            if (document.getElementById("err")) {
                [...document.querySelectorAll("span")].forEach(e => {
                    e.remove();
                });
            }

            const btn = document.getElementById("btn");
            const error = document.createElement("span");
            error.innerHTML = "Нужно ввести одинаковые значения";
            error.style.color = "red";
            error.id = "err";
            document.getElementById("btn").before(error);
            btn.before(error);
        }
        e.preventDafault();
    }
})
