"use strict";

// 1st variant
// let newArr = [];

// function filterBy(arr, typeOfData) {
//     arr.forEach(element => {
//         if (typeof element !== typeOfData) {
//             newArr.push(element);
//         };
//     });
//     return newArr;
// };
// console.log(filterBy(['hello', 'world', 23, '23', null], "string"));


// 2nd variant (BEST)
function filterBy(array, typeOfData) {
    return array.filter(element => typeof element !== typeOfData);
};

console.log(filterBy(['hello', 'world', 23, '23', null], "string"));
