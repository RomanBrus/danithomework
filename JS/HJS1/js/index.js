"use strict";

const userName = prompt("Input your name");
const userAge = +prompt("Input your age?");

if (userAge < 18) {
    alert("You are not allowed to visit this website");
} else if (userAge > 18 && userAge <= 22) {
    const result = confirm("Are you sure you want to continue?");
    if (result) {
        alert(`"Welcome, ${userName}"`);
    } else {
        alert("You are not allowed to visit this website");
    }
} else {
    alert(`"Welcome, ${userName}"`);
}

