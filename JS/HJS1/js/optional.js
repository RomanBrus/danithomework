"use strict";

let userName;
let userAge;

do {
    userName = prompt("Input your name", userName);
    userAge = prompt("Input your age?", userAge);
} while
    (!userName ||
    !(userAge && !isNaN(userAge) && typeof +userAge === "number"));

if (userAge < 18) {
    alert("You are not allowed to visit this website");
} else if (userAge > 18 && userAge <= 22) {
    let result = confirm("Are you sure you want to continue?");
    if (result) {
        alert(`"Welcome, ${userName}"`);
    } else {
        alert("You are not allowed to visit this website");
    }
} else {
    alert(`"Welcome, ${userName}"`);
}




