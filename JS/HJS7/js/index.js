"use strict";

function viewList(arr, parent = document.body) {
    const htmlElArr = arr.map((e) => `<li>${e}</li>`);
    const li = htmlElArr.join("");
    parent.insertAdjacentHTML('afterbegin', `<ul>${li}</ul>`);
};

viewList(["1", "2", "3", "sea", "user", 23]);





