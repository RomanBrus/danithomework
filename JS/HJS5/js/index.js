"use strict";

function createNewUser() {
    const userName = prompt("Enter your name");
    const userSurname = prompt("Enter your surname");
    const userAge = prompt("Enter your age in format 'dd.mm.yyyy' ");
    
    const newUser = {
        "firstName": userName,
        "lastName": userSurname,
        "birthday": userAge,

        getLogin : function (userName, userSurname) {
            const shortName = this["firstName"].toLowerCase().slice(0,1) + this["lastName"].toLowerCase();        
            return shortName;
        },

        getAge : function (userAge) {
            const age = (new Date()).getFullYear() - this["birthday"].slice(-4);        
            return age;
        },

        getPassword : function (userName, userSurname) {
            const pass = this["firstName"].toUpperCase().slice(0, 1) + this["lastName"].toLowerCase() + 
            this["birthday"].slice(-4);        
            return pass;
        },
    }

    return newUser;
}

const user = createNewUser();

console.log(user.getLogin(),
    user.getAge(),
    user.getPassword(),);

