"use strict";

const switcher = document.getElementById("switcher");
let theme = "light";
let themeColor = document.getElementById("themeMode");

if (localStorage.getItem(`themeMode`) == "light") {
    themeColor.href = "./css/light_style.css";
} else {
    themeColor.href = "./css/dark_style.css";
}

switcher.addEventListener("click", function () {
    if (themeColor.getAttribute("href") == "./css/light_style.css") {
        themeColor.href = "./css/dark_style.css";
        theme = "dark";
        window.localStorage.setItem(`themeMode`, theme);
    } else {
        window.localStorage.removeItem(`themeMode`, theme);
        themeColor.href = "./css/light_style.css";
        theme = "light";
        window.localStorage.setItem(`themeMode`, theme);
    }
})



