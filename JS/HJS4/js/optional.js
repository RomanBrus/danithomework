"use strict";

function createNewUser() {
    
    const _userName = prompt("Enter your name");
    const _userSurname = prompt("Enter your surname");
    const newUser = {
        "firstName": _userName,
        "lastName": _userSurname,

        getLogin : function (userName, userSurname) {
            const shortName = this["firstName"].toLowerCase().slice(0,1) + this["lastName"].toLowerCase();        
            return shortName;
        },

        setFirstName(value) {
            return this._userName = value;
        },

        setLastName(value) {
            return this._userSurname = value;
        },
    }

    Object.defineProperties(newUser, {
        'userName': {
        writable: false
    },
        'userSurname': {
        writable: false
    }
    });

    return newUser;
}

console.log(createNewUser().getLogin());