"use strict";

function createNewUser() {
    
    const userName = prompt("Enter your name");
    const userSurname = prompt("Enter your surname");
    const newUser = {
        "firstName": userName,
        "lastName": userSurname,
        getLogin : function (userName, userSurname) {
            const shortName = this["firstName"].toLowerCase().slice(0,1) + this["lastName"].toLowerCase();        
            return shortName;
        }
    }

    return newUser;
}

console.log(createNewUser().getLogin());