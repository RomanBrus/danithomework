"use strict";
const images = document.querySelectorAll("img");
let curImg = 0;
const stop = document.getElementById("stop");
stop.addEventListener("click", pause)
const start = document.getElementById("start");
start.addEventListener("click", begin);
let go = setInterval(slideShow, 3000);
let showing = true;

function slideShow() {
    images[curImg].className = "image-to-show";
    curImg = (curImg + 1) % images.length;
    images[curImg].className = "image-to-show active";
}

function pause() {
    clearInterval(go);
    showing = false;
}

function begin() {
    if (!showing) {
        go = setInterval(slideShow, 3000);
        showing = true;
    }
}




