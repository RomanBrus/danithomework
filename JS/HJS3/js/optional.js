"use strict";

let num1;
let num2;
let symb;

do {
    num1 = +prompt("Enter number 1", num1);
    num2 = +prompt("Enter number 2", num2);
    symb = prompt("Enter operation", symb);
} while (
    !(num2 && !isNaN(num2) && typeof num2 === "number") &&
    !(num1 && !isNaN(num1) && typeof num1 === "number")
);

function resOperation(num1, num2, symb) {
    let res;
    
    switch (symb) {
        case "+":
            res = num1 + num2;
            break;

        case "*":
            res = num1 * num2;
            break;

        case "/":
            res = num1 / num2;
            break;

        case "-":
            res = num1 - num2;
            break;
    }

    return res;
}

console.log(resOperation(num1, num2, symb));