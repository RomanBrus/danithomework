"use strict";

let num1 = +prompt("Enter number 1");
let num2 = +prompt("Enter number 2");
let symb = prompt("Enter operation");

function resOperation(num1, num2, symb) {
    let res;

    switch (symb) {
        case "+":
            res = num1 + num2;
            break;

        case "*":
            res = num1 * num2;
            break;

        case "/":
            res = num1 / num2;
            break;

        case "-":
            res = num1 - num2;
            break;
    }

    return res;
}

console.log(resOperation(num1, num2, symb));