"use strict";

const num = +prompt("Enter number");

if (num < 5) {
    console.log("Sorry, no numbers");
} else {
    for (let index = 1; index < num; index++) {
        if (index % 5 == 0) {
            console.log(index);
        };
    }
}
