"use strict";

document.addEventListener('keydown', logKey);
function logKey(e) {
    const btns = [...document.querySelectorAll(".btn")];

    btns.forEach(el => {
        el.classList.remove("active");
        if (el.dataset.btn == e.code) {
            el.classList.add("active");
        }
    });
}

