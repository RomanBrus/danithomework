"use strict";

const tabsBtn = document.querySelectorAll(".tabs-title");
const tabsItems = document.querySelectorAll(".tabs-item");

tabsBtn.forEach(function (title) {
    title.addEventListener("click", function () {
        let thisBtn = title;
        let thisId = thisBtn.getAttribute("data-tab");
        let currentTab = document.querySelector(thisId);

        if (!thisBtn.classList.contains("active")) {
            tabsBtn.forEach(function (title) {
                title.classList.remove('active');
            })

            tabsItems.forEach(function (title) {
                title.classList.remove('active');
            })

            thisBtn.classList.add('active');
            currentTab.classList.add("active");
        }
    })
})