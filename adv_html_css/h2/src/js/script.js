const btn = document.querySelector(".btn");
const btnLines = document.querySelector(".btn__lines");
const navigation = document.querySelector(".navigation");

btn.addEventListener("click", function(e){
    btnLines.classList.toggle("active");
    navigation.classList.toggle("active");
});

