import { Component } from "react";

import "./App.css";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

class App extends Component {
  state = {
    firstModalDisplay: false,
    secondModalDisplay: false,
  };

  openFirstModal = () => {
    this.setState({ firstModalDisplay: true });
  };

  closeFirstModal = () => {
    this.setState({ firstModalDisplay: false });
  };

  openSecondModal = () => {
    this.setState({ secondModalDisplay: true });
  };

  closeSecondModal = () => {
    this.setState({ secondModalDisplay: false });
  };

  render() {
    const { firstModalDisplay, secondModalDisplay } = this.state;

    return (
      <div className="app__buttons">
        <Button
          backgroundColor="red"
          text="Open first modal"
          className="modalButton"
          onClick={this.openFirstModal}
        />
        <Button
          backgroundColor="green"
          text="Open second modal"
          className="modalButton"
          onClick={this.openSecondModal}
        />
        {firstModalDisplay && (
          <Modal
            className="modal"
            modalDisplay={firstModalDisplay}
            text="First modal some text"
            closeButton={true}
            close={this.closeFirstModal}
            header="First modal header"
            actions={[
              <Button
                key={1}
                onClick={this.closeFirstModal}
                className="modalButton"
                text="Ok"
              />,
              <Button
                key={2}
                onClick={this.closeFirstModal}
                className="modalButton"
                text="Cancel"
              />,
            ]}
          />
        )}
        {secondModalDisplay && (
          <Modal
            modalDisplay={secondModalDisplay}
            text="Second modal some text"
            closeButton={true}
            close={this.closeSecondModal}
            header="Second modal header"
            actions={[
              <Button
                key={3}
                onClick={this.closeSecondModal}
                className="modalButton"
                text="NoOk"
              />,
              <Button
                key={4}
                onClick={this.closeSecondModal}
                className="modalButton"
                text="NoCancel"
              />,
            ]}
          />
        )}
      </div>
    );
  }
}

export default App;
