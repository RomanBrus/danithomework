import React, { Component } from "react";
import styles from "./Button.module.scss";
// import styles from "../Modal/Modal.module.scss";

class Button extends React.PureComponent {
  render() {
    const { backgroundColor, text, onClick, className } = this.props;

    return (
      <button
        className={styles.modalButton}
        style={{ backgroundColor }}
        onClick={onClick}
      >
        {text}
      </button>
    );
  }
}

export default Button;
