import React, { Children } from "react";
import styles from "./Modal.module.scss";

class Modal extends React.PureComponent {
  render() {
    const { closeButton, text, actions, close, firstModalDisplay } = this.props;
    console.log(firstModalDisplay);
    if (firstModalDisplay) {
      return null;
    }
    return (
      <div className={styles.modalBackground} onClick={close}>
        <div
          onClick={(e) => {
            e.stopPropagation();
          }}
          className={styles.modalContent}
        >
          <header className={styles.modalContentHeader}>
            {closeButton && (
              <div onClick={close} className={styles.modalCloseBtn}>
                &times;
              </div>
            )}
          </header>
          <div>
            <p className={styles.modalContentText}>{text}</p>
            <div className={styles.modalButtons}>{actions}</div>
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;
