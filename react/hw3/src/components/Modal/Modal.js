import React from "react";
import styles from "./Modal.module.scss";

const Modal = ({
  closeButton,
  text,
  actions,
  close,
  modalProps,
  addToCart,
  setModalProps,
}) => {
  return (
    <div className={styles.modalBackground} onClick={close}>
      <div
        onClick={(e) => {
          e.stopPropagation();
        }}
        className={styles.modalContent}
      >
        <header className={styles.modalContentHeader}>
          {closeButton && (
            <div onClick={close} className={styles.modalCloseBtn}>
              &times;
            </div>
          )}
        </header>
        <div>
          <p className={styles.modalContentText}>{text}</p>
          <div className={styles.modalButtons}>{actions}</div>
        </div>
      </div>
    </div>
  );
};

export default Modal;
