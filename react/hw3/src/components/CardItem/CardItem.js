import styles from "./CardItem.module.scss";
import Button from "../Button/Button";
import starIcon from "../../components/icons/star_favorite_favorite_favorite_favorite_2359.png";
import outlineStarIcon from "../../components/icons/services_favorite_favorite_star_9891.png";
import PropTypes from "prop-types";

const CardItem = (props) => {
  const {
    item,
    favourite,
    openFirstModal,
    setModalProps,
    addToFavourites,
    toCart,
    fromCart,
  } = props;
  const { name, path, price, article } = item;

  return (
    <div className={styles.card}>
      <button type="button" className={styles.likeButton}>
        <img
          onClick={() => {
            setModalProps(article);
            addToFavourites(article);
          }}
          src={favourite ? outlineStarIcon : starIcon}
          alt="Favourite"
        />
      </button>
      <span className={styles.title}>{name}</span>
      <img className={styles.itemAvatar} src={path} alt={name} />
      <span className={styles.description}>{price}</span>

      <div className={styles.btnContainer}>
        <div className="app__buttons">
          {toCart && (
            <Button
              backgroundColor="green"
              text="Add to cart"
              className="modalButton"
              onClick={() => {
                setModalProps({ article, name });
                openFirstModal("Add to cart");
              }}
            />
          )}
          {fromCart && (
            <Button
              backgroundColor="red"
              className="modalButton"
              onClick={() => {
                setModalProps({ article, name });
                openFirstModal("Remove from cart");
              }}
              text={"X"}
            />
          )}
        </div>
      </div>
    </div>
  );
};

export default CardItem;

CardItem.propTypes = {
  name: PropTypes.string,
  path: PropTypes.string,
  price: PropTypes.string,
  article: PropTypes.string,
  color: PropTypes.string,
  favourite: PropTypes.bool,
  openFirstModal: PropTypes.func,
  setModalProps: PropTypes.func,
  addToFavourites: PropTypes.func,
};
