import React from "react";
import { useState, useEffect } from "react";
import "./App.css";
import Button from "./components/Button/Button";
import Header from "./components/Header/Header";
import Modal from "./components/Modal/Modal";
import { BrowserRouter } from "react-router-dom";
import AppRoutes from "./AppRoutes.js";

const App = () => {
  const [firstModalDisplay, setfirstModalDisplay] = useState(false);
  const [favourites, setFavourites] = useState([]);
  const [cards, setCards] = useState([]);
  const [carts, setCarts] = useState([]);
  const [modalProps, setModalProps] = useState({});
  const [modalAction, setModalAction] = useState("Add to cart");

  useEffect(() => {
    (async () => {
      const cards = await fetch(`./items.json`).then((res) => res.json());
      cards.forEach((card) => {
        card.isFavourite =
          localStorage.getItem("Favourites") &&
          JSON.parse(localStorage.getItem("Favourites")).includes(card.article);
      });
      setCards(cards);
      if (localStorage.getItem("carts")) {
        const carts = await JSON.parse(localStorage.getItem("carts"));
        setCarts(carts);
      }
      if (localStorage.getItem("favourites")) {
        const favourites = await JSON.parse(localStorage.getItem("favourites"));
        setFavourites(favourites);
      }
    })();
  }, []);

  const openFirstModal = (text) => {
    if (text === "Add to cart") {
      setModalAction("Add to cart");
    }
    if (text === "Remove from cart") {
      setModalAction("Remove from cart");
    }
    setfirstModalDisplay(true);
  };

  const closeFirstModal = () => {
    setfirstModalDisplay(false);
  };

  const addToCart = (article) => {
    setCarts((current) => {
      const carts = [...current];
      const index = carts.findIndex((el) => el === article);

      if (index === -1) {
        carts.push(...article);
      } else {
        carts[index].count += 1;
      }

      localStorage.setItem("carts", JSON.stringify(carts));
      closeFirstModal();
      return carts;
    });
  };

  const deleteThisItem = (article) => {
    setCarts((current) => {
      const carts = [...current];

      const index = carts.findIndex((el) => el === article);

      if (index !== -1) {
        carts.splice(index, 1);
      }

      localStorage.setItem("carts", JSON.stringify(carts));
      closeFirstModal();
      return carts;
    });
  };

  const addToFavourites = (article) => {
    setFavourites((current) => {
      let favourites = [...current];
      if (current.includes(article)) {
        favourites = current.filter((el) => {
          return el !== article;
        });
      } else {
        favourites = [...current, article];
      }
      localStorage.setItem("favourites", JSON.stringify(favourites));
      closeFirstModal();
      return favourites;
    });
  };

  return (
    <BrowserRouter>
      <>
        <Header carts={carts} favourites={favourites} />
        <section>
          <AppRoutes
            deleteThisItem={deleteThisItem}
            favourites={favourites}
            addToCart={addToCart}
            cards={cards}
            carts={carts}
            openFirstModal={openFirstModal}
            setModalProps={setModalProps}
            addToFavourites={addToFavourites}
            modalAction={modalAction}
          />
        </section>

        {firstModalDisplay && (
          <Modal
            text={`${modalAction} '${modalProps.name}'?`}
            className="modal"
            firstModalDisplay={firstModalDisplay}
            closeButton={true}
            close={closeFirstModal}
            modalProps={modalProps}
            addToCart={addToCart}
            setModalProps={setModalProps}
            actions={[
              <Button
                key={1}
                onClick={() => {
                  if (modalAction === "Add to cart") {
                    addToCart(modalProps.article);
                  }
                  if (modalAction === "Remove from cart") {
                    deleteThisItem(modalProps.article);
                  }
                }}
                className="modal-button"
                text="Yes"
              />,
              <Button
                key={2}
                onClick={closeFirstModal}
                className="modal-button"
                text="Cancel"
              />,
            ]}
          />
        )}
      </>
    </BrowserRouter>
  );
};

export default App;
