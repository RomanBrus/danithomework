import React from "react";
import styles from "./Button.module.scss";
import PropTypes from "prop-types";

const Button = ({ backgroundColor, text, onClick, type }) => {
  return (
    <button
      type={type}
      className={styles.modalButton}
      style={{ backgroundColor }}
      onClick={onClick}
    >
      {text}
    </button>
  );
};

export default Button;

Button.propTypes = {
  text: PropTypes.string,
  onClick: PropTypes.func,
};

Button.defaultProps = {
  text: "",
  onClick: () => {},
};
