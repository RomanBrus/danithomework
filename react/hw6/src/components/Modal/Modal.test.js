import { render, fireEvent, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import Modal from "./Modal";

jest.mock("react-redux", () => ({
  useSelector: () => {
    return { info: "testAction", data: { name: "itemName" } };
  },
}));

const idModal = "modalJest";
const idButton = "closeButtonJest";

describe("Modal.js", () => {
  test("should Modal Render", () => {
    render(<Modal className="modalBackground" />);
  });

  test("Try modal Action", () => {
    const action = ["Action"];
    render(<Modal actions={action} />);
  });

  test("Close Modal", () => {
    const closeModal = jest.fn();
    const { getByTestId } = render(<Modal closeModal={closeModal} />);
    fireEvent.click(screen.getByTestId(idModal));
    expect(closeModal).toHaveBeenCalled();
  });

  test("Close Button", () => {
    const closeModal = jest.fn();
    const { getByTestId } = render(
      <Modal closeModal={closeModal} closeButton />
    );
    fireEvent.click(screen.getByTestId(idButton));
    expect(closeModal).toHaveBeenCalled();
  });

  test("stopPropagation", () => {
    const { getByTestId } = render(<Modal />);
    const modalBody = getByTestId("modalJest");
    expect(modalBody).toBeInTheDocument();
    fireEvent.click(modalBody);
  });

  test("Snapshot", () => {
    const { container } = render(<Modal />);
    expect(container.innerHTML).toMatchSnapshot();
  });
});
