import styles from "./Header.module.scss";
import cartIcon from "../../components/icons/cart-outline.svg";
import favIcon from "../../components/icons/services_favorite_favorite_star_9891.png";
import { NavLink } from "react-router-dom";
import { useSelector, shallowEqual } from "react-redux";

const Header = () => {
  const items = useSelector((state) => state.items.data, shallowEqual);
  const favourites = items.filter((item) => item.favourite);
  const trashItems = items.filter((item) => item.inTrashAmount);

  let cartQt;
  let favouriteQt;
  if (trashItems.length) {
    cartQt = <span>{trashItems.length}</span>;
  } else {
    cartQt = <span>0</span>;
  }

  if (favourites.length) {
    favouriteQt = <span>{favourites.length}</span>;
  } else {
    favouriteQt = <span>0</span>;
  }

  return (
    <header className={styles.root}>
      <ul>
        <li>
          <NavLink to="/">MAIN</NavLink>
        </li>
        <li>
          <NavLink to="/cart">
            Cart
            <img src={cartIcon} alt="Cart" />
          </NavLink>
          {cartQt}
        </li>
        <li>
          <NavLink to="/favourites">
            Favourites
            <img src={favIcon} alt="Cart" width={26} />
            {favouriteQt}
          </NavLink>
        </li>
      </ul>
    </header>
  );
};

export default Header;
