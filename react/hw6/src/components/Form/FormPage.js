import React from "react";
import styles from "./Form.module.scss";
import Button from "../Button/Button";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as yup from "yup";
import { setTrash, getTrash } from "../../store/operations";
import { REMOVE_FROM_TRASH, MAKE_ORDER } from "../../store/types";
import { useDispatch } from "react-redux";
import { useSelector, shallowEqual } from "react-redux";

const FormPage = () => {
  const validationSchema = yup.object().shape({
    name: yup
      .string()
      .min(3, "Min 3 symbols")
      .required("Name field is requierd"),
    surname: yup
      .string()
      .min(3, "Min 3 symbols")
      .required("Surname field is requierd"),
    age: yup.number().required("Age field is requierd").positive().integer(),
    adress: yup.string().required("Adress field is requierd"),
    phone: yup.number().required("Phone field is requierd").positive(),
  });

  const dispatch = useDispatch();
  const items = useSelector((state) => state.items.data, shallowEqual);

  const removeFromTrash = (data) => {
    dispatch({ type: REMOVE_FROM_TRASH, payload: data });
  };

  const makeOrder = (data) => {
    dispatch({ type: MAKE_ORDER, payload: data });
  };

  const handleSubmit = (values, { resetForm }) => {
    const data = getTrash();
    const order = items.filter((item) => item.inTrashAmount);
    makeOrder(order);
    Object.keys(data).forEach((element) => {
      removeFromTrash(element);
    });
    console.log(order);
    setTrash({});
    console.log(values);
    resetForm();
  };

  const initialValues = {
    name: "",
    surname: "",
    age: "",
    adress: "",
    phone: "",
  };

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={handleSubmit}
      validationSchema={validationSchema}
    >
      {({ dirty, isValid, handleSubmit }) => {
        return (
          <Form>
            <Field type="text" name="name" placeholder="Name" />
            <ErrorMessage name="name">
              {(msg) => <span className={styles.error}>{msg}</span>}
            </ErrorMessage>

            <Field type="text" name="surname" placeholder="Surname" />
            <ErrorMessage name="surname">
              {(msg) => <span className={styles.error}>{msg}</span>}
            </ErrorMessage>

            <Field type="text" name="age" placeholder="Age" />
            <ErrorMessage name="age">
              {(msg) => <span className={styles.error}>{msg}</span>}
            </ErrorMessage>

            <Field type="text" name="adress" placeholder="Adress" />
            <ErrorMessage name="adress">
              {(msg) => <span className={styles.error}>{msg}</span>}
            </ErrorMessage>

            <Field type="phone" name="phone" placeholder="Phone" />
            <ErrorMessage name="phone">
              {(msg) => <span className={styles.error}>{msg}</span>}
            </ErrorMessage>

            <Button
              type="submit"
              text="Checkout"
              className={styles.form__button}
            ></Button>
          </Form>
        );
      }}
    </Formik>
  );
};

export default FormPage;
