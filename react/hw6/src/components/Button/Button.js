import React, { memo } from "react";
import styles from "./Button.module.scss";
import PropTypes from "prop-types";

const Button = ({ backgroundColor, text, onClick, type }) => {
  return (
    <button
      type={type}
      data-testid="closeButtonJest"
      className={styles.modalButton}
      style={{ backgroundColor }}
      onClick={onClick}
    >
      {text}
    </button>
  );
};

export default memo(Button);

Button.propTypes = {
  text: PropTypes.string,
  onClick: PropTypes.func,
};

Button.defaultProps = {
  text: "",
  onClick: () => {},
};
