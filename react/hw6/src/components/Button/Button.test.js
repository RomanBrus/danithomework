import { render, screen, fireEvent } from "@testing-library/react";
import Button from "./Button";

const onClick = jest.fn();

describe("Render Button", () => {
  test("should button render", () => {
    const { asFragment } = render(
      <Button type={"submit"} backgroundColor="blue" text="Submit">
        SUBMIT
      </Button>
    );
    expect(asFragment()).toMatchSnapshot();
  });
});

describe("Handle click at a button", () => {
  test("should onClick work", () => {
    render(
      <Button
        type={"submit"}
        backgroundColor="blue"
        text="Submit"
        onClick={onClick}
      ></Button>
    );
    const btn = screen.getByText("Submit");
    fireEvent.click(btn);
    expect(onClick).toHaveBeenCalled();
  });
});
