import React from "react";
import CardItem from "../../components/CardItem";
import styles from "./FavouritesPage.module.scss";
import { useSelector, shallowEqual } from "react-redux";

const FavouritesPage = () => {
  const items = useSelector((state) => state.items.data, shallowEqual);
  const favourites = items.filter((item) => item.favourite);

  if (!JSON.parse(localStorage.getItem("Favourite"))) {
    return <h2 className="container">Empty</h2>;
  }

  return (
    <>
      <h2>Favourites</h2>
      <div className={styles.favourite}>
        {favourites.map((item) => {
          return <CardItem key={item.path} item={item} toCart />;
        })}
      </div>
    </>
  );
};

export default FavouritesPage;
