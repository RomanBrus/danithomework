import notFoundImg from "../../assets/images/not_found.jpeg";
import styles from "./NotFoundPage.module.scss";
import Button from "../../components/Button/Button";
import { useNavigate, useLocation } from "react-router-dom";

const NotFoundPage = () => {
  const { pathname } = useLocation();
  const navigate = useNavigate();

  return (
    <div>
      <h2 className={styles.title}>
        {" "}
        This page <span className={styles.path}>{pathname}</span> is not found{" "}
      </h2>
      <img className={styles.notFoundImg} src={notFoundImg} alt="not-found" />
      <Button
        onClick={() => {
          navigate("/");
        }}
      >
        Back to Home
      </Button>
    </div>
  );
};

export default NotFoundPage;
